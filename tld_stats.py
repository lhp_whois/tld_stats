#!/usr/bin/env python3
import argparse, os, sys
import datetime
import mysql.connector
import json

from configobj import ConfigObj
from collections import namedtuple

# ===================================================
# Global Data
# ===================================================
script_base_dir = os.path.dirname(os.path.realpath(__file__))
config_file = script_base_dir + "/stats_summary.conf"
config_dict = ConfigObj(config_file)
max_version_range = 10000
latest_gtld_version = None
latest_cctld_version = None

SummaryQueryInfo = namedtuple('SummaryQueryInfo', \
                              ["domain_names_total", "whois_records_total", "table_found"])

with open('/net/webnx7/var/www/vhosts/domainwhoisdatabase.com/whois_database/tlds.json', 'rt') as tlds_json:
    gtlds_catalog = json.load(tlds_json)

with open('/net/webnx7/var/www/vhosts/domainwhoisdatabase.com/domain_list_quarterly/tlds.json', 'rt') as tlds_json:
    cctlds_catalog = json.load(tlds_json)


def query_latest_gtld_version(conf_dict):
    latest_release = sorted(gtlds_catalog['releases'].keys(), key=lambda r: int(r[1:]))[-1]
    latest_version = int(latest_release[1:])
    return latest_version


def query_latest_cctld_version(conf_dict):
    latest_release = sorted(cctlds_catalog['releases'].keys(), key=lambda r: int(r[1:]))[-1]
    latest_version = int(latest_release[1:])
    return latest_version


def determine_tld_type(tld):
    tld_type = None
    if tld in gtlds_catalog['tlds'].keys():
        tld_type = 'gtld'
    elif tld in cctlds_catalog['tlds'].keys():
        tld_type = 'cctld'
    return tld_type


def generate_and_print_daily_summary(tld, today_date, conf_dict, option='registered'):
    # Take average whois records and domain names for past 7 days
    tld_type = determine_tld_type(tld)
    if tld_type == "" or tld_type is None:
        print('{tld} does not exist'.format(tld=tld))
        return None
    time_stamp_format = "%Y_%m_%d"
    # pretty_time_stamp_format = "%B %d, %Y"

    # Gather mysql data for domain_names and whois records from 6 days before this
    time_stamp_db_str = today_date.strftime(time_stamp_format)
    # print today_date.strftime(time_stamp_format)

    if tld_type == 'cctld':
        domain_names_count, whois_records_count, table_found = perform_daily_sql_query(tld, tld_type, conf_dict, time_stamp_db_str, option=option)
    else:
        domain_names_count, whois_records_count, table_found = perform_daily_sql_query(tld, tld_type, conf_dict, time_stamp_db_str)
        if domain_names_count == 0:
            # Daily ngtld
            domain_names_count, whois_records_count, table_found = perform_daily_sql_query(tld, 'ngtld', conf_dict, time_stamp_db_str)

    # if not (table_found):
    #     print('table not found')
    #     return None
    print(tld)
    print('domain names: {}'.format(domain_names_count))
    print('whois records: {}\n'.format(whois_records_count))


def perform_sql_query(version, tld, tld_type, conf_dict, with_sub_tlds):
    if with_sub_tlds:
        # Get TLDs like ***.***.uk
        tlds = get_sub_tlds(version, tld, tld_type, config_dict)
    else:
        tlds = [tld, ]

    mysql_cnx = mysql.connector.connect(**conf_dict["webnx2_db"])
    db_cursor = mysql_cnx.cursor()

    domain_names_total = 0
    whois_records_total = 0
    table_found = False

    for tld in tlds:
        # Vesion 1 and 2 use old table names
        if tld_type == 'cctld' and version <= 2:
            domain_names_query = \
                "SELECT COUNT(*) FROM {0}v{1}_{2}.{3};".format( \
                    conf_dict["cctld_quarterly"]["db_name_prefix"], \
                    version, tld.replace('.', '_'), \
                    conf_dict["cctld_quarterly"]["legacy_domain_names_table"])
        else:
            domain_names_query = \
                "SELECT COUNT(*) FROM {0}v{1}_{2}.{3};".format( \
                    conf_dict["%s_quarterly" % tld_type]["db_name_prefix"], \
                    version, tld.replace('.', '_'), \
                    conf_dict["cctld_quarterly"]["domain_names_table"])

        whois_records_query = \
            "SELECT COUNT(*) FROM {0}v{1}_{2}.{3};".format( \
                conf_dict["%s_quarterly" % tld_type]["db_name_prefix"], \
                version, tld.replace('.', '_'), \
                conf_dict["%s_quarterly" % tld_type]["whois_record_table"])

        # print domain_names_query
        try:
            db_cursor.execute(domain_names_query)
            for count in db_cursor:
                # print count
                domain_names_total += count[0]
            table_found = True
        except mysql.connector.Error as err:
            if args.debug:
                print(err)

        # print whois_records_query
        try:
            db_cursor.execute(whois_records_query)
            for count in db_cursor:
                # print count
                whois_records_total += count[0]
            table_found = True
        except mysql.connector.Error as err:
            if args.debug:
                print(err)

    db_cursor.close()
    mysql_cnx.close()
    return SummaryQueryInfo(domain_names_total, whois_records_total, table_found)

def tld_exists_in_version(tld, v, catalog):
    return (v in catalog['tlds'][tld]['records']['csv']) and (v in catalog['tlds'][tld]['records']['whois_record'])

def generate_and_print_quarterly_summary(tlds, valid_versions, with_sub_tlds):
    for version in valid_versions:
        print("version: {}\n".format(version))
        for tld in tlds:
            tld_type = determine_tld_type(tld)
            if tld_type == "" or tld_type is None:
                print('unknown tld: {}'.format(tld))
                continue
            if tld_type == "gtld":
                catalog = gtlds_catalog
            elif tld_type == "cctld":
                catalog = cctlds_catalog
            else:
                continue
            if not tld_exists_in_version(tld, version, catalog):
                print('{tld} not available in {version}'.format(tld=tld, version=version))
                continue
            print("{}\ncsv records: {}\nwhois records: {}\n".format(
                tld,
                catalog['tlds'][tld]['records']['csv'][version],
                catalog['tlds'][tld]['records']['whois_record'][version]
            ))

def get_quarterly_sum_stats(tlds):
    res = {}
    for tld in tlds:
        tld_type = determine_tld_type(tld)
        if tld_type == "" or tld_type is None:
            print('unknown tld: {}'.format(tld))
            continue
        if tld_type == "gtld":
            try:
                csv_dict = gtlds_catalog['tlds'][tld]['records']['csv']
            except:
                csv_dict = {}
            try:
                whois_dict = gtlds_catalog['tlds'][tld]['records']['whois_record']
            except:
                whois_dict = {}
        elif tld_type == "cctld":
            try:
                csv_dict = cctlds_catalog['tlds'][tld]['records']['csv']
            except:
                csv_dict = {}
            try:
                whois_dict = cctlds_catalog['tlds'][tld]['records']['whois_record']
            except:
                whois_dict = {}
        else:
            continue
        csv_records = sum([v for v in csv_dict.values()])
        whois_records = sum([v for v in whois_dict.values()])
        main_tld = tld.split('.')[-1]
        if main_tld in res:
            res[main_tld]['csv_records'] += csv_records
            res[main_tld]['whois_records'] += whois_records
        else:
            res[main_tld] = {}
            res[main_tld]['csv_records'] = csv_records
            res[main_tld]['whois_records'] = whois_records
    return res


def parse_arguments():
    arg_parser = argparse.ArgumentParser(description="Generate summary for specified tld")
    arg_parser.add_argument("-t", "--tld", required=True, type=str,
                            help="Summary will be given for this TLD")
    arg_parser.add_argument("--with-sub-tlds", action='store_true',
                            help="Result will have sub TLDs too. ")
    # arg_parser.add_argument("-nd", "--no-show-daily-data", action="store_true",
    #                         help="Do not display daily data")
    # arg_parser.add_argument("-nq", "--no-show-quarterly-data", action="store_true",
    #                         help="Do not display quarterly data")
    arg_parser.add_argument(
        "-v", "--db-version",
        type=str, default="",
        help="Version you would like to display. Can comma delimit. Example: v1,v2,v3")
    arg_parser.add_argument("--sum", action="store_true", help="Sum domains of all versions")
    arg_parser.add_argument("-uv", "--up-to-db-version", action="store_true",
                            help="Specifying this parameter means you want from v1 to --db-version")
    arg_parser.add_argument("--debug", action="store_true",
                            help="Display debug messages")
    arg_parser.add_argument("--n-days", type=int, help="last <number> days")
    arg_parser.add_argument("--daily-registered", action="store_true", help="print daily registered domains only")
    arg_parser.add_argument("--daily-discovered", action="store_true", help="print daily discovered domains only")
    args = arg_parser.parse_args()

    # Strip '.' incase it is provided
    # args.tld = args.tld.lstrip(".")
    args.tld = list(map(lambda x: x.strip('.'), args.tld.split(',')))
    return args


def perform_daily_sql_query(tld, tld_type, conf_dict, time_stamp, option='registered'):

    tld_main = tld[tld.rfind('.') + 1:]
    mysql_host = ''
    domain_query_list = []
    whois_query_list = []

    if tld_type == 'cctld':
        if option == 'registered':
            mysql_host = "webnx2_db"
            # Query strings for newly registered
            query = "SELECT count(*) FROM " + conf_dict["cctld_daily"]["db_newly_registered_name"] + \
                    "." + conf_dict["cctld_daily"]["newly_registered_domain_name_table_prefix"] + tld_main + "_add_" + \
                    time_stamp + " WHERE domain_name LIKE '%." + tld + "';"
            domain_query_list.append(query)

            query = "SELECT count(*) FROM " + conf_dict["cctld_daily"]["db_newly_registered_name"] + \
                    "." + conf_dict["cctld_daily"]["newly_registered_whois_record_table_prefix"] + tld_main + "_add_" + \
                    time_stamp + " WHERE domain_name LIKE '%." + tld + "';"
            whois_query_list.append(query)

        # Query for newly discovered
        else:
            mysql_host = "webnx3_db"
            query = "SELECT count(*) FROM " + conf_dict["cctld_daily"]["db_newly_discovered_name"] + \
                    "." + conf_dict["cctld_daily"]["newly_discovered_domain_name_table_prefix"] + tld_main + "_add_" + \
                    time_stamp + " WHERE domain_name LIKE '%." + tld + "';"
            domain_query_list.append(query)

            query = "SELECT count(*) FROM " + conf_dict["cctld_daily"]["db_newly_discovered_name"] + \
                    "." + conf_dict["cctld_daily"]["newly_discovered_whois_record_table_prefix"] + tld_main + "_add_" + \
                    time_stamp + " WHERE domain_name LIKE '%." + tld + "';"
            whois_query_list.append(query)
    else:
        mysql_host = "webnx3_db"

        query = \
            "SELECT count(*) FROM {0}.{1}{2}_add_{3}".format( \
                conf_dict["%s_daily" % tld_type]["db_name"], \
                conf_dict["%s_daily" % tld_type]["domain_name_table_prefix"], \
                tld, \
                time_stamp)
        domain_query_list.append(query)

        query = \
            "SELECT count(*) FROM {0}.{1}{2}_add_{3}".format( \
                conf_dict["%s_daily" % tld_type]["db_name"], \
                conf_dict["%s_daily" % tld_type]["whois_record_table_prefix"], \
                tld, \
                time_stamp)
        whois_query_list.append(query)

    if args.debug:
        print('host: ', conf_dict[mysql_host]['host'])
        print('domain_query_list: ', domain_query_list)
        print('whois_query_list: ', whois_query_list)


    mysql_connection = mysql.connector.connect(**conf_dict[mysql_host])
    db_cursor = mysql_connection.cursor()

    domain_names_total = 0
    whois_records_total = 0
    table_found = False

    for query in domain_query_list:
        try:
            db_cursor.execute(query)
            for count in db_cursor:
                domain_names_total += count[0]
            table_found = True
        except mysql.connector.Error as err:
            if args.debug:
                print(err)

    for query in whois_query_list:
        try:
            db_cursor.execute(query)
            for count in db_cursor:
                whois_records_total += count[0]
            table_found = True
        except mysql.connector.Error as err:
            if args.debug:
                print(err)

    db_cursor.close()
    mysql_connection.close()
    return SummaryQueryInfo(domain_names_total, whois_records_total, table_found)


def get_sub_tlds(v, tld, tld_type, conf_dict):
    mysql_connection = mysql.connector.connect(**conf_dict["webnx2_db"])
    db_cursor = mysql_connection.cursor()

    query = \
        "SHOW DATABASES LIKE \"{0}v{1}_%{2}\";".format(conf_dict["%s_quarterly" % tld_type]['db_name_prefix'], v, tld.replace('.', '_'))

    try:
        db_cursor.execute(query)
        tlds = db_cursor.fetchall()
    except mysql.connector.Error as err:
        pass

    tld_list = []
    for t in tlds:
        t = t[0]
        s = '_v%s_' % v
        t = t[t.find(s) + len(s):]
        if t.endswith('_%s' % tld.replace('.', '_')) or t == tld.replace('.', '_'):
            tld_list.append(t)

    db_cursor.close()
    mysql_connection.close()
    return tld_list


args = parse_arguments()

latest_gtld_version = query_latest_gtld_version(config_dict)
latest_cctld_version = query_latest_cctld_version(config_dict)

# tld_type = ""

# Determine if version is valid
if (len(args.db_version) > 0):
    version_split = args.db_version.split(",")
else:
    version_split = args.db_version

# valid_versions = list()
if (args.up_to_db_version):
    up_to_version = list()
    for i in range(1, int(version_split[0].strip("v")) + 1):
        up_to_version.append("v" + str(i))
    version_split = up_to_version

if not (args.db_version or args.n_days):
    if args.tld[0] == 'cctlds':
        tlds = sorted(cctlds_catalog['tlds'].keys())
    elif args.tld[0] == 'gtlds':
        tlds = sorted(gtlds_catalog['tlds'].keys())
    else:
        print('invalid arguments')
        sys.exit(1)
    res = get_quarterly_sum_stats(tlds)
    main_tlds = res.keys()
    main_tlds = list(filter(lambda tld: res[tld]['csv_records'] > 0 or res[tld]['whois_records'] > 0, main_tlds))
    for main_tld in sorted(main_tlds):
        print(main_tld)
        print('csv_records: {}'.format(res[main_tld]['csv_records']))
        print('whois_records: {}\n'.format(res[main_tld]['whois_records']))

if args.db_version:
    print("=================================\n",
          "Quarterly TLD Summary\n",
          "\r=================================")
    generate_and_print_quarterly_summary(
        args.tld, version_split, args.with_sub_tlds)

if args.n_days:
    print("=================================\n",
          "Daily TLD Summary\n",
          "\r=================================")

    if not args.daily_registered:
        print("------------------------------------\n",
              "Discovered\n",
              "\r------------------------------------")
        today_date = datetime.date.today()
        for i in range(0, args.n_days):
            today_date = today_date - datetime.timedelta(days=1)
            print("Date: {year}-{month}-{day}".format(year=today_date.year, month=today_date.month, day=today_date.day))
            for tld in args.tld:
                generate_and_print_daily_summary(tld, today_date, config_dict,option='discovered')
            print('\n')
    if not args.daily_discovered:
        print("------------------------------------\n",
              "Registered\n",
              "\r------------------------------------")
        today_date = datetime.date.today()
        for i in range(0, args.n_days):
            today_date = today_date - datetime.timedelta(days=1)
            print("Date: {year}-{month}-{day}".format(year=today_date.year, month=today_date.month, day=today_date.day))
            for tld in args.tld:
                generate_and_print_daily_summary(tld, today_date, config_dict,option='registered')
            print('\n')
