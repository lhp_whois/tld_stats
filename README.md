# README #

This script is to show the statistics of daily and quarterly domain feeds.

### Requirements ###

python3

### Installation ###

git clone git@bitbucket.org:lhp_whois/tld_stats.git

### Usage ###

* help  
  ./tld_stats.py -h
* show stats for quarterly feed of domains  
./tld_stats.py -t <tlds delimited by commas> -v <data feed version>  
e.g.  
./tld_stats.py -t com.hk,org.hk,gov.hk,edu.hk,net.hk,idv.hk,hk -v v13  
* show stats for daily feed of domains  
./tld_stats.py -t <tlds delimited by commas> --n-days <last n days>  
e.g.  
./tld_stats.py -t com,org --n-days 3

### Who do I talk to? ###
contact: tech.support@whoisxmlapi.com
